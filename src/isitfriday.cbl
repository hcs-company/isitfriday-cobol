IDENTIFICATION DIVISION.
PROGRAM-ID. IsItFriday.
AUTHOR. Wander Boessenkool.

DATA DIVISION.
WORKING-STORAGE SECTION.
01 DOW PIC 9(1).
01 NTS PIC 9(1).
01 TMP PIC 9(1).
01 OVR PIC 9(3).
01 QS PIC x(255).
01 QSKEY PIC x(3).
01 QSVAL PIC 9(3).

PROCEDURE DIVISION.
  ACCEPT DOW FROM DAY-OF-WEEK.
  COMPUTE TMP = FUNCTION MOD((12 - DOW), 7).
  ACCEPT QS FROM ENVIRONMENT "QUERY_STRING".
  UNSTRING QS DELIMITED BY "="
    INTO QSKEY, QSVAL
  END-UNSTRING.
  IF QSKEY = "nts"
  THEN
    COMPUTE NTS = FUNCTION MOD(QSVAL, 7)
  ELSE
    COMPUTE NTS = TMP
  END-IF.
  
  DISPLAY "Content-type: text/html" & x"0a".
  DISPLAY "<!doctype html>"
          "<html>"
          "  <head>"
          "    <title>Is It Friday? (COBOL Edition)</title>"
          "    <link rel='stylesheet' href='css/style.css'/>"
          "  </head>"
          "  <body>"
          "    <div class='question'>"
          "      Is It Friday?"
          "    </div>"
  END-DISPLAY.
  IF NTS > 0
  THEN
    DISPLAY "<div class='answer no'>NO</div>"
            "<div class='nts' id='nts'>Just " NTS " more"
    END-DISPLAY
    IF NTS > 1
    THEN
      DISPLAY "nights"
    ELSE
      DISPLAY "night"
    END-IF
    DISPLAY "of sleep to go.</div>"
  ELSE
    DISPLAY "<div class='answer yes' id='answer'>YES!</div>"
            "<div class='nts' id='nts'></div>"
    END-DISPLAY
  END-IF.


  DISPLAY "  </body>"
          "</html>"
  END-DISPLAY.

  STOP RUN.
