# Is It Friday, now in COBOL!

## Enjoy all your weekly weekend needs with a hint of '60s and '70s flair!

### Modern Computing Using Tested Languages!


#### Running This Piece of Pure Unadulterated Bliss:

```bash
podman run --rm -it -p 8080:8080 registry.gitlab.com/hcs-company/isitfriday-cobol:latest
```
